export const productList = [
  {
    id: 1,
    productName: "Samsung Galaxy A23",
    productPrice: 149000,
    productImage:
      "https://cdn.tgdd.vn/Products/Images/42/262650/samsung-galaxy-a23-cam-thumb-600x600.jpg",
  },
  {
    id: 2,
    productName: "Samsung Galaxy S23 Ultra 5",
    productPrice: 159000,
    productImage:
      "https://cdn.tgdd.vn/Products/Images/42/249948/samsung-galaxy-s23-ultra-1-600x600.jpg",
  },
  {
    id: 3,
    productName: "OPPO Reno8 T 5G”",
    productPrice: 139000,
    productImage:
      "https://cdn.tgdd.vn/Products/Images/42/301641/oppo-reno8t-den1-thumb-600x600.jpg",
  },
  {
    id: 4,
    productName: "Samsung Galaxy S20 FE (8GB/256GB)",
    productPrice: 259000,
    productImage:
      "https://cdn.tgdd.vn/Products/Images/42/224859/samsung-galaxy-s20-fan-edition-xanh-la-thumbnew-600x600.jpeg",
  },
  {
    id: 5,
    productName: "Samsung Galaxy A23",
    productPrice: 149000,
    productImage:
      "https://cdn.tgdd.vn/Products/Images/42/262650/samsung-galaxy-a23-cam-thumb-600x600.jpg",
  },
  {
    id: 6,
    productName: "Samsung Galaxy S23 Ultra 5",
    productPrice: 159000,
    productImage:
      "https://cdn.tgdd.vn/Products/Images/42/249948/samsung-galaxy-s23-ultra-1-600x600.jpg",
  },
  {
    id: 7,
    productName: "OPPO Reno8 T 5G”",
    productPrice: 139000,
    productImage:
      "https://cdn.tgdd.vn/Products/Images/42/301641/oppo-reno8t-den1-thumb-600x600.jpg",
  },
  {
    id: 8,
    productName: "Samsung Galaxy S20 FE (8GB/256GB)",
    productPrice: 259000,
    productImage:
      "https://cdn.tgdd.vn/Products/Images/42/224859/samsung-galaxy-s20-fan-edition-xanh-la-thumbnew-600x600.jpeg",
  },
];
