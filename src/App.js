import React from "react";
import Login from './components/auth/Login';
import { Route, Routes } from "react-router-dom";
import "./App.css";
import LayoutPage from "./components/LayoutPage";
import Ecommerce from "./page/Ecommerce";

function App() {
  return (
    <Routes>
      <Route path="/" element={<LayoutPage />} />
      <Route path="/shopping" element={<Ecommerce />} />
      <Route path="/login" element={<Login />} />
    </Routes>
  );
}

export default App;
