import axios from "axios";
import { baseUrl } from "../components/constants/constants";

const getUserList = async () => {
  let response = [];
  await axios.get(`${baseUrl}/members/`).then((res) => {
    response = res.data;
  });
  return response;
};

const createUser = async (formData) => {
  let response = null;
  await axios.post(`${baseUrl}/create/`, formData).then((res) => {
    response = res;
  });
  return response;
};

const deleteUser = async (id) => {
  let response = null;
  await axios.delete(`${baseUrl}/members/${id}/`).then((res) => {
    response = res;
  });
  return response;
}

const getLogs = async () => {
  let response = [];
  await axios.get(`${baseUrl}/logs/`).then((res) => {
    response = res.data;
  });
  return response;
};

const getDoorStatus = async () => {
  let response = null;
  await axios.get(`${baseUrl}/door/status`).then((res) => {
    response = res.data;
  })
  return response;
}

const lockDoor = async () => {
  let response = null;
  await axios.get(`${baseUrl}/door/lock/`).then((res) => {
    response = res.data;
  })
  return response;
}

const unlockDoor = async () => {
  let response = null;
  await axios.get(`${baseUrl}/door/unlock/`).then((res) => {
    response = res.data;
  })
  return response;
}

export { getUserList, createUser, deleteUser, getLogs, getDoorStatus, lockDoor, unlockDoor };
