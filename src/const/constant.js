export const USER_ROLE_ADMIN = 'ADMIN'
export const USER_ROLE_MEMBER = 'MEMBER'

export const ACTION_VIEW_PAGE_1 = 'VIEW_PAGE_1'
export const ACTION_VIEW_PAGE_2 = 'VIEW_PAGE_2'