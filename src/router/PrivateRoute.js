import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { hasPermission } from './permissions'
import { USER_ROLE_ADMIN } from './constants'

function PrivateRoute({ component: Component, requiredAction, ...rest }) {
  const userRole = useSelector(state => state.auth.userRole)

  return (
    <Route
      {...rest}
      render={props =>
        hasPermission(userRole, requiredAction) ? (
          <Component {...props} />
        ) : userRole === USER_ROLE_ADMIN ? (
          <Redirect to="/admin" />
        ) : (
          <Redirect to="/" />
        )
      }
    />
  )
}

export default PrivateRoute