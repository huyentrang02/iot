import { ACTION_VIEW_PAGE_1, ACTION_VIEW_PAGE_2 } from '../const/constant'
import { USER_ROLE_ADMIN, USER_ROLE_MEMBER } from './constants'

export const PERMISSIONS = {
  [USER_ROLE_ADMIN]: { [ACTION_VIEW_PAGE_1]: true, [ACTION_VIEW_PAGE_2]: true },
  [USER_ROLE_MEMBER]: { [ACTION_VIEW_PAGE_1]: true, [ACTION_VIEW_PAGE_2]: false }
}

export function hasPermission(userRole, actionType) {
  if (PERMISSIONS[userRole] && PERMISSIONS[userRole][actionType]) {
    return true
  } else {
    return false
  }
}