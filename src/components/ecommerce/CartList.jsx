import { Avatar, Input, List } from "antd";
import { DeleteOutlined } from "@ant-design/icons";
import React, { useEffect } from "react";
import { useState } from "react";

export default function CardList({ cartList, handleChangeQuantity }) {
  const [total, setTotal] = useState(0);
  const getTotal = () => {
    let newTotal = 0;
    cartList.forEach((item) => {
      console.log(item);
      newTotal += item.productPrice * item.quantity;
    });
    setTotal(newTotal);
  };

  useEffect(() => {
    getTotal();
  }, [cartList]);

  return (
    <List
      header={<h2>Giỏ hàng</h2>}
      itemLayout="horizontal"
      dataSource={cartList}
      renderItem={(item) => (
        <List.Item actions={[<DeleteOutlined />]}>
          <List.Item.Meta
            avatar={<Avatar shape="square" size={64} src={item.productImage} />}
            title={item.productName}
            description={item.productPrice}
          />
          <Input
            type="number"
            style={{ width: 90 }}
            value={item.quantity}
            onChange={(e) => handleChangeQuantity(e, item.id)}
          />
        </List.Item>
      )}
      footer={!!cartList.length && <Footer total={total} />}
    />
  );
}

const Footer = ({ total }) => {
  return (
    <div className="cart-total">
      <h3>Tổng</h3>
      <span>
        {new Intl.NumberFormat("vi-VN", {
          style: "currency",
          currency: "VND",
        }).format(total)}
      </span>
    </div>
  );
};
