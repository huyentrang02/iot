import { Card } from "antd";
import React from "react";
import { PlusOutlined } from "@ant-design/icons";

const { Meta } = Card;

export default function ProductItem({ productDetail, handleAddToCart }) {
  const { productName, productPrice, productImage } = productDetail;

  return (
    <Card
      hoverable
      style={{ width: 240 }}
      cover={<img alt="example" src={productImage} />}
      actions={[<AddToCart handleAddToCart={handleAddToCart} />]}
    >
      <Meta
        title={productName}
        description={new Intl.NumberFormat("vi-VN", {
          style: "currency",
          currency: "VND",
        }).format(productPrice)}
      />
    </Card>
  );
}

const AddToCart = ({handleAddToCart}) => {
  const handleClick = () => {
    console.log("click");
  };
  return (
    <div onClick={handleAddToCart}>
      <PlusOutlined style={{ width: "unset" }} /> Add to cart
    </div>
  );
};
