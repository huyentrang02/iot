import {
  CheckOutlined,
  DeleteOutlined,
  DingtalkOutlined,
  PlusOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import {
  Button,
  Form,
  Input,
  InputNumber,
  message,
  Modal,
  Select,
  Table,
} from "antd";
import { Option } from "antd/lib/mentions";
import { default as React, useState } from "react";
import { createUser, deleteUser } from "../api/UserApi";

export default function PageList() {
  const [openModel, setOpenModel] = useState(false);
  // const [listUser, setListUser] = useState([]);

  const [form] = Form.useForm();

  const userTableColumn = [
    { title: "Mã sản phẩm", dataIndex: "id", key: "id" },
    { title: "Tên sản phẩm", dataIndex: "name", key: "name" },
    { title: "Số lượng", dataIndex: "total", key: "quantity" },
    { title: "Giá bán", dataIndex: "price", key: "total" },
    { title: "Mô tả", dataIndex: "des", key: "des" },
    {
      title: "Thao tác",
      dataIndex: "id",
      key: "delete",
      render: (id) => (
        <div>
          <Button
            type="danger"
            style={{ marginRight: "20px" }}
            onClick={() => handleDeleteUser(id)}
          >
            <DeleteOutlined />
            Delete
          </Button>
          <Button
            type="primary"
            style={{ marginRight: "20px" }}
            onClick={() => showModal(id)}
          >
            <CheckOutlined />
            Edit
          </Button>
        </div>
      ),
    },
  ];

  const showModal = () => {
    setOpenModel(true);
  };

  const handleCancel = () => {
    setOpenModel(false);
    form.resetFields();
  };

  // const getUserListApi = async () => {
  //   const data = await getUserList();
  //   setListUser(data.members);
  // };

  const deleteUserApi = async (id) => {
    const res = await deleteUser(id);
    if (res.data.error === 0) {
      const newListUser = listUser.filter((user) => user.id !== id);
      // setListUser(newListUser);
      message.success("Delete Success");
    } else {
      message.error("Delete faild");
    }
  };

  const handleDeleteUser = async (id) => {
    const deleteUser = listUser.find((user) => user.id === id);
    const warningModel = Modal.confirm();
    warningModel.update({
      title: "Xác nhận xoá",
      icon: <DeleteOutlined />,
      content: `Bạn có chắc chắn muốn xoá ${deleteUser.name}?`,
      cancelText: "Huỷ",
      okText: "Xoá",
      okType: "danger",
      onOk: () => deleteUserApi(id),
      onCancel: () => warningModel.destroy(),
    });
  };

  const handleBrowseMenu = async (id) => {
    // const browseMenu = listUser.find((user) => user.id === id);
    // const warningModel = Modal.confirm();
    // warningModel.update({
    //   title: "Sửa",
    //   icon: <CheckOutlined />,
    //   content: `Bạn có chắc chắn muốn duyệt đơn ${deleteUser.name}?`,
    //   cancelText: "Huỷ",
    //   okText: "Duyệt",
    //   okType: "primary",
    //   onOk: () => deleteUserApi(id),
    //   onCancel: () => warningModel.destroy(),
    // });
  };

  // useEffect(() => {
  //   getUserListApi();
  // }, []);

  const handleSubmit = async (values) => {
    let formData = new FormData();
    formData.append("avatar", values.avatar.fileList[0].originFileObj);
    formData.append("name", values.name);
    const res = await createUser(formData);
    if (res.data.error === 0) {
      // setListUser([...listUser, res.data]);
      message.success("Upload Success");
    } else {
      message.error("Upload faild");
    }
    handleCancel();
  };
  let listUser = [
    {
      id: "1",
      name: "Sản phẩm 1",
      total: 20,
      price: "2.000.0000",
      des: "note",
    },
    {
      id: "1",
      name: "Sản phẩm 2",
      total: 30,
      price: "2.899.0000",
      des: "note",
    },
  ];
  return (
    <div>
      <div style={{ marginBottom: "20px" }}>
        <Button onClick={showModal} type="dashed">
          <PlusOutlined /> Thêm sản phẩm
        </Button>
      </div>
      <Form onFinish={() => {}} layout="inline" form={form}>
        <Form.Item
          name="keyword"
          style={{
            flexGrow: 1,
            marginRight: "10px",
            marginBottom: "50px",
          }}
          rules={[
            {
              required: true,
              message: "Nhập sản phẩm muốn tìm kiếm",
            },
            {
              whitespace: true,
              message: "Không được bỏ trống!!",
            },
          ]}
        >
          <Input
            size="large"
            placeholder="Nhập sản phẩm muốn tìm kiếm"
            prefix={<DingtalkOutlined />}
          />
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            size="large"
            htmlType="submit"
            icon={<SearchOutlined />}
            style={{
              backgroundColor: "#CC5200",
              border: "none",
              padding: "0px 20px",
            }}
          >
            <span>Tìm kiếm</span>
          </Button>
        </Form.Item>
      </Form>
      <Table
        // title="Danh sách sản phẩm"
        columns={userTableColumn}
        dataSource={listUser}
        bordered={true}
        rowKey="id"
      />

      <Modal
        title="Thêm sản phẩm"
        open={openModel}
        onCancel={handleCancel}
        maskClosable={false}
        onOk={() => {
          form
            .validateFields()
            .then((values) => {
              form.resetFields();
              handleSubmit(values);
            })
            .catch((info) => {
              console.log("Validate Failed:", info);
            });
        }}
      >
        <Form
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 14 }}
          layout="horizontal"
          onFinish={handleSubmit}
          form={form}
        >
          <Form.Item
            label="Tên sản phẩm"
            name="name"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tên!",
              },
            ]}
          >
            <Input placeholder="Nhập tên sản phẩm" />
          </Form.Item>
          <Form.Item
            label="Số lượng"
            name="quantity"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tổng số sản phẩm!",
              },
            ]}
          >
            <InputNumber
              placeholder="Nhập tổng số sản phẩm"
              style={{ width: "100%" }}
            />
          </Form.Item>
          <Form.Item
            label="Giá bán"
            name="price"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập giá bán sản phẩm!",
              },
            ]}
          >
            <InputNumber
              placeholder="Nhập giá bán sản phẩm"
              style={{ width: "100%" }}
            />
          </Form.Item>
          <Form.Item
            label="Mô tả"
            name="price"
            // rules={[
            //   {
            //     required: true,
            //     message: "Vui lòng nhập giá bán sản phẩm!",
            //   },
            // ]}
          >
            <Input
              placeholder="Nhập mô tả sản phẩm"
              style={{ width: "100%" }}
            />
          </Form.Item>

          {/* <Form.Item
            label="Image"
            name="avatar"
            rules={[
              {
                required: true,
                message: "Vui lòng chọn ảnh!",
              },
            ]}
          >
            <Upload listType="picture" maxCount={1} beforeUpload={() => false}>
              <Button icon={<UploadOutlined />}>Image</Button>
            </Upload>
          </Form.Item> */}
        </Form>
      </Modal>
    </div>
  );
}
