import { Table } from "antd";
import { default as React, useEffect, useState } from "react";
import { baseUrl } from "./constants/constants";
import { getLogs } from "../api/UserApi";

export default function PageList({socket}) {
  const [logData, setLogData] = useState([]);
  const logTableColumn = [
    { title: "ID", dataIndex: "id", key: "id", sorter: (a, b) => a.id > b.id },
    { title: "Name", dataIndex: "name", key: "name" },
    {
      title: "Avatar",
      dataIndex: "avatar",
      key: "avatar",
      render: (avatar) => (
        <img
          src={`${baseUrl}${avatar}`}
          alt={avatar}
          style={{ height: "100px" }}
        />
      ),
    },
    {
      title: "Capture",
      dataIndex: "capture",
      key: "capture",
      render: (capture) => (
        <img
          src={`${baseUrl}${capture}`}
          alt={capture}
          style={{ height: "100px" }}
        />
      ),
    },
    {
      title: "Created",
      dataIndex: "created",
      key: "created",
      sorter: (a, b) => new Date(a.created) > new Date(b.created),
    },
  ];

  const getLogsApi = async () => {
    const data = await getLogs();
    setLogData(data.logs.reverse());
  };

  useEffect(() => {
    getLogsApi();
  }, []);

  useEffect(() => {
    socket.on("message", () => {
      getLogsApi();
    });

    return () => {
      socket.off("message");
    };
  }, [socket]);

  return (
    <div>
      <Table columns={logTableColumn} dataSource={logData} bordered={true} rowKey="id" />
    </div>
  );
}
