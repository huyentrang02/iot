import {
  CheckOutlined,
  DeleteOutlined,
  DingtalkOutlined,
  PlusOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import {
  Button,
  Form,
  Input,
  InputNumber,
  message,
  Modal,
  Select,
  Table,
} from "antd";
import { Option } from "antd/lib/mentions";
import { default as React, useState } from "react";
import { createUser, deleteUser } from "../api/UserApi";

export default function PageList() {
  const [openModel, setOpenModel] = useState(false);
  // const [listUser, setListUser] = useState([]);

  const [form] = Form.useForm();

  const userTableColumn = [
    { title: "Mã nhà cung cấp", dataIndex: "id", key: "id" },
    { title: "Tên nhà cung cấp", dataIndex: "name", key: "name" },
    { title: "Số điện thoại", dataIndex: "phoneNumber", key: "phoneNumber" },
    { title: "Email", dataIndex: "email", key: "email" },
    { title: "Địa chỉ", dataIndex: "address", key: "address" },
    {
      title: "Thao tác",
      dataIndex: "id",
      key: "delete",
      render: (id) => (
        <div>
          <Button
            type="danger"
            style={{ marginRight: "20px" }}
            onClick={() => handleDeleteUser(id)}
          >
            <DeleteOutlined />
            Delete
          </Button>
          <Button
            type="primary"
            style={{ marginRight: "20px" }}
            onClick={() => showModal(id)}
          >
            <CheckOutlined />
            Edit
          </Button>
        </div>
      ),
    },
  ];

  const showModal = () => {
    setOpenModel(true);
  };

  const handleCancel = () => {
    setOpenModel(false);
    form.resetFields();
  };

  // const getUserListApi = async () => {
  //   const data = await getUserList();
  //   setListUser(data.members);
  // };

  const deleteUserApi = async (id) => {
    const res = await deleteUser(id);
    if (res.data.error === 0) {
      const newListUser = listUser.filter((user) => user.id !== id);
      // setListUser(newListUser);
      message.success("Delete Success");
    } else {
      message.error("Delete faild");
    }
  };

  const handleDeleteUser = async (id) => {
    const deleteUser = listUser.find((user) => user.id === id);
    const warningModel = Modal.confirm();
    warningModel.update({
      title: "Xác nhận xoá",
      icon: <DeleteOutlined />,
      content: `Bạn có chắc chắn muốn xoá ${deleteUser.name}?`,
      cancelText: "Huỷ",
      okText: "Xoá",
      okType: "danger",
      onOk: () => deleteUserApi(id),
      onCancel: () => warningModel.destroy(),
    });
  };

  const handleBrowseMenu = async (id) => {
    // const browseMenu = listUser.find((user) => user.id === id);
    // const warningModel = Modal.confirm();
    // warningModel.update({
    //   title: "Sửa",
    //   icon: <CheckOutlined />,
    //   content: `Bạn có chắc chắn muốn duyệt đơn ${deleteUser.name}?`,
    //   cancelText: "Huỷ",
    //   okText: "Duyệt",
    //   okType: "primary",
    //   onOk: () => deleteUserApi(id),
    //   onCancel: () => warningModel.destroy(),
    // });
  };

  // useEffect(() => {
  //   getUserListApi();
  // }, []);

  const handleSubmit = async (values) => {
    let formData = new FormData();
    formData.append("avatar", values.avatar.fileList[0].originFileObj);
    formData.append("name", values.name);
    const res = await createUser(formData);
    if (res.data.error === 0) {
      // setListUser([...listUser, res.data]);
      message.success("Upload Success");
    } else {
      message.error("Upload faild");
    }
    handleCancel();
  };
  let listUser = [
    {
      id: "1",
      name: "nhà cung cấp 1",
      phoneNumber: "0967123456",
      email: "abc@gmail.com",
      address: "Hà Nội",
    },
    {
      id: "1",
      name: "nhà cung cấp 2",
      phoneNumber: "0967123456",
      email: "abc@gmail.com",
      address: "TP Hồ Chí Minh",
    },
  ];
  return (
    <div>
      <div style={{ marginBottom: "20px" }}>
        <Button onClick={showModal} type="dashed">
          <PlusOutlined /> Thêm nhà cung cấp
        </Button>
      </div>
      <Form onFinish={() => {}} layout="inline" form={form}>
        <Form.Item
          name="keyword"
          style={{
            flexGrow: 1,
            marginRight: "10px",
            marginBottom: "50px",
          }}
          rules={[
            {
              required: true,
              message: "Nhập nhà cung cấp muốn tìm kiếm",
            },
            {
              whitespace: true,
              message: "Không được bỏ trống!!",
            },
          ]}
        >
          <Input
            size="large"
            placeholder="Nhập nhà cung cấp muốn tìm kiếm"
            prefix={<DingtalkOutlined />}
          />
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            size="large"
            htmlType="submit"
            icon={<SearchOutlined />}
            style={{
              backgroundColor: "#CC5200",
              border: "none",
              padding: "0px 20px",
            }}
          >
            <span>Tìm kiếm</span>
          </Button>
        </Form.Item>
      </Form>
      <Table
        // title="Danh sách nhà cung cấp"
        columns={userTableColumn}
        dataSource={listUser}
        bordered={true}
        rowKey="id"
      />

      <Modal
        title="Thêm nhà cung cấp"
        open={openModel}
        onCancel={handleCancel}
        maskClosable={false}
        onOk={() => {
          form
            .validateFields()
            .then((values) => {
              form.resetFields();
              handleSubmit(values);
            })
            .catch((info) => {
              console.log("Validate Failed:", info);
            });
        }}
      >
        <Form
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 14 }}
          layout="horizontal"
          onFinish={handleSubmit}
          form={form}
        >
          <Form.Item
            label="Tên nhà cung cấp"
            name="name"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tên!",
              },
            ]}
          >
            <Input placeholder="Nhập tên nhà cung cấp" />
          </Form.Item>
          <Form.Item
            label="Số điện thoại"
            name="name"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập số điện thoại!",
              },
            ]}
          >
            <Input placeholder="Nhập số điện thoại" />
          </Form.Item>
          <Form.Item
            label="Email"
            name="name"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập email!",
              },
            ]}
          >
            <Input placeholder="Nhập email" />
          </Form.Item>
          <Form.Item
            label="Địa chỉ"
            name="address"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập địa chỉ!",
              },
            ]}
          >
            <Input placeholder="Nhập địa chỉ" />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
