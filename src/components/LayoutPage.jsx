import { AppstoreOutlined, MailOutlined } from "@ant-design/icons";
import { Layout, Menu } from "antd";
import React, { useState } from "react";
import QLDH from "./QLDH";
import QLSP from "./QLSP";
import QLNCC from "./QLNCC";
import DoanhThu from "./ThongKe/DoanhThu";
import KhachHang from "./ThongKe/KhachHang";
import SanPhamBanDuoc from "./ThongKe/SanPhamBanDuoc";
import SanPhamNhapVao from "./ThongKe/SanPhamNhapVao";
import TKNhaPhanPhoi from "./NhapHangOnline/TKNhaPhanPhoi";
import TKSanPham from "./NhapHangOnline/TKSanPham";
import DonNhapOnline from "./NhapHangOnline/DonNhapOnline";
import Login from "./auth/Login";

import "../App.css";
import { Route, Routes } from "react-router-dom";

const { Sider, Content } = Layout;

function LayoutPage() {
  const [selectedKeys, setSelectedKeys] = useState("1");

  function getItem(label, key, icon, children, type) {
    return {
      key,
      icon,
      children,
      label,
      type,
    };
  }

  const items = [
    getItem("Quản lý đơn hàng", "1", <MailOutlined />),
    getItem("Quản lý sản phẩm", "2", <MailOutlined />),
    getItem("Quản lý nhà cung cấp", "3", <MailOutlined />),
    getItem("Thống kê", "sub1", <AppstoreOutlined />, [
      getItem("Doanh thu", "4"),
      getItem("Khách hàng", "5"),
      getItem("Sản phẩm bán được", "6"),
      getItem("Sản phẩm nhập vào", "7"),
    ]),
    getItem("Nhập hàng", "8", <AppstoreOutlined />),
  ];

  const onClick = (e) => {
    setSelectedKeys(e.key);
  };

  const handleChangeSelectedKeys = (key) => {
    setSelectedKeys(key);
  };

  const renderSwitchTab = () => {
    switch (selectedKeys) {
      case "1":
        return <QLDH />;
      case "2":
        return <QLSP />;
      case "3":
        return <QLNCC />;
      case "4":
        return <DoanhThu />;
      case "5":
        return <KhachHang />;
      case "6":
        return <SanPhamBanDuoc />;
      case "7":
        return <SanPhamNhapVao />;
      case "8":
        return <TKNhaPhanPhoi handleChangeSelectedKeys={handleChangeSelectedKeys} />;
      case "9":
        return <TKSanPham handleChangeSelectedKeys={handleChangeSelectedKeys} />;
      case "10":
        return <DonNhapOnline />;
      default:
        break;
    }
  };

  return (
    <Layout
      style={{ minHeight: "100vh" }}
      id="components-layout-demo-custom-trigger"
    >
      <Sider trigger={null} collapsible>
        <Menu
          theme="dark"
          mode="inline"
          selectedKeys={selectedKeys}
          items={items}
          onClick={onClick}
        />
      </Sider>
      <Layout className="site-layout">
        <Content
          className="site-layout-background"
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          {renderSwitchTab()}
        </Content>
      </Layout>
    </Layout>
  );
}

export default LayoutPage;
