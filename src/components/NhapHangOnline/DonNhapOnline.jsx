import {
  CheckOutlined,
  DeleteOutlined,
  DingtalkOutlined,
  PlusOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { Button, Form, Input, message, Modal, Table } from "antd";
import { default as React, useState } from "react";
// import { createUser, deleteUser } from "..api/UserApi";

export default function PageList() {
  const [openModel, setOpenModel] = useState(false);
  // const [listUser, setListUser] = useState([]);

  const [form] = Form.useForm();

  const userTableColumn = [
    { title: "STT", dataIndex: "id", key: "id" },
    { title: "Tên sản phẩm", dataIndex: "name", key: "name" },
    { title: "Đơn giá", dataIndex: "price", key: "price" },
    { title: "Số lượng", dataIndex: "quantity", key: "quantity" },
  ];

  const showModal = () => {
    setOpenModel(true);
  };

  const handleCancel = () => {
    setOpenModel(false);
    form.resetFields();
  };

  // const getUserListApi = async () => {
  //   const data = await getUserList();
  //   setListUser(data.members);
  // };

  const deleteUserApi = async (id) => {
    // const res = await deleteUser(id);
    // if (res.data.error === 0) {
    //   const newListUser = listUser.filter((user) => user.id !== id);
    //   // setListUser(newListUser);
    //   message.success("Delete Success");
    // } else {
    //   message.error("Delete faild");
    // }
  };

  const handleDeleteUser = async (id) => {
    const deleteUser = listUser.find((user) => user.id === id);
    const warningModel = Modal.confirm();
    warningModel.update({
      title: "Xác nhận xoá",
      icon: <DeleteOutlined />,
      content: `Bạn có chắc chắn muốn xoá ${deleteUser.name}?`,
      cancelText: "Huỷ",
      okText: "Xoá",
      okType: "danger",
      onOk: () => deleteUserApi(id),
      onCancel: () => warningModel.destroy(),
    });
  };

  const handleBrowseMenu = async () => {};

  // const handleSubmit = async (values) => {};
  let listUser = [
    {
      id: "1",
      name: "Sản phẩm 1",
      price: "2.000.000",
      quantity: 30,
    },
    {
      id: "2",
      name: "Sản phẩm 2",
      price: "5.000.000",
      quantity: 10,
    },
  ];
  return (
    <div>
      <p style={{ fontSize: "20px" }}>
        <b>Thông tin đơn hàng</b>
      </p>
      <p>Tên nhà phân phối: </p>
      <p>Thời gian đặt hàng: </p>
      <p>Tổng tiền </p>
      <p>Chi tiết: </p>

      <Table
        // title="Danh sách đơn hàng"
        columns={userTableColumn}
        dataSource={listUser}
        bordered={true}
        rowKey="id"
      />
      <Button
        type="primary"
        style={{ marginRight: "20px" }}
        onClick={() => handleBrowseMenu()}
      >
        <CheckOutlined />
        Xác nhận
      </Button>
    </div>
  );
}
