import {
  CheckOutlined,
  DeleteOutlined,
  DingtalkOutlined,
  PlusOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { Button, Form, Input, message, Modal, Table } from "antd";
import { default as React, useState } from "react";
// import { createUser, deleteUser } from "..api/UserApi";

export default function PageList({ handleChangeSelectedKeys }) {
  const [openModel, setOpenModel] = useState(false);
  // const [listUser, setListUser] = useState([]);

  const [form] = Form.useForm();

  const userTableColumn = [
    { title: "STT", dataIndex: "id", key: "id" },
    { title: "Tên sản phẩm", dataIndex: "name", key: "name" },
    {
      title: "Thao tác",
      dataIndex: "action",
      key: "delete",
      render: (id) => (
        <div>
          <Button
            type="danger"
            style={{ marginRight: "20px" }}
            onClick={() => handleDeleteUser(id)}
          >
            <DeleteOutlined />
            Delete
          </Button>
          <Button
            type="primary"
            style={{ marginRight: "20px" }}
            onClick={() => handleChangeSelectedKeys("10")}
          >
            <CheckOutlined />
            Chọn
          </Button>
        </div>
      ),
    },
  ];

  const showModal = () => {
    setOpenModel(true);
  };

  const handleCancel = () => {
    setOpenModel(false);
    form.resetFields();
  };

  // const getUserListApi = async () => {
  //   const data = await getUserList();
  //   setListUser(data.members);
  // };

  const deleteUserApi = async (id) => {
    // const res = await deleteUser(id);
    // if (res.data.error === 0) {
    //   const newListUser = listUser.filter((user) => user.id !== id);
    //   // setListUser(newListUser);
    //   message.success("Delete Success");
    // } else {
    //   message.error("Delete faild");
    // }
  };

  const handleDeleteUser = async (id) => {
    const deleteUser = listUser.find((user) => user.id === id);
    const warningModel = Modal.confirm();
    warningModel.update({
      title: "Xác nhận xoá",
      icon: <DeleteOutlined />,
      content: `Bạn có chắc chắn muốn xoá ${deleteUser.name}?`,
      cancelText: "Huỷ",
      okText: "Xoá",
      okType: "danger",
      onOk: () => deleteUserApi(id),
      onCancel: () => warningModel.destroy(),
    });
  };

  const handleBrowseMenu = async (id) => {
    // const browseMenu = listUser.find((user) => user.id === id);
    // const warningModel = Modal.confirm();
    // warningModel.update({
    //   title: "Chọn nhà phân phối",
    //   icon: <CheckOutlined />,
    //   content: `Bạn có chắc chắn muốn chọn nhà phân phối ${deleteUser.name}?`,
    //   cancelText: "Huỷ",
    //   okText: "Chọn",
    //   okType: "primary",
    //   onOk: () => deleteUserApi(id),
    //   onCancel: () => warningModel.destroy(),
    // });
  };

  // useEffect(() => {
  //   getUserListApi();
  // }, []);

  const handleSubmit = async (values) => {
    // let formData = new FormData();
    // formData.append("avatar", values.avatar.fileList[0].originFileObj);
    // formData.append("name", values.name);
    // const res = await createUser(formData);
    // if (res.data.error === 0) {
    //   // setListUser([...listUser, res.data]);
    //   message.success("Upload Success");
    // } else {
    //   message.error("Upload faild");
    // }
    // handleCancel();
  };
  let listUser = [
    {
      id: "1",
      name: "Sản phẩm 1",
    },
    {
      id: "2",
      name: "Sản phẩm 2",
    },
  ];
  return (
    <div>
      <div style={{ marginBottom: "20px" }}>
        <Button onClick={showModal} type="dashed">
          <PlusOutlined /> Thêm sản phẩm
        </Button>
      </div>
      <Form onFinish={() => {}} layout="inline" form={form}>
        <Form.Item
          name="keyword"
          style={{
            flexGrow: 1,
            marginRight: "10px",
            marginBottom: "50px",
          }}
          rules={[
            {
              required: true,
              message: "Nhập sản phẩm muốn tìm kiếm",
            },
            {
              whitespace: true,
              message: "Không được bỏ trống!!",
            },
          ]}
        >
          <Input
            size="large"
            placeholder="Nhập sản phẩm muốn tìm kiếm"
            prefix={<DingtalkOutlined />}
          />
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            size="large"
            htmlType="submit"
            icon={<SearchOutlined />}
            style={{
              backgroundColor: "#CC5200",
              border: "none",
              padding: "0px 20px",
            }}
          >
            <span>Tìm kiếm</span>
          </Button>
        </Form.Item>
      </Form>
      <Table
        // title="Danh sách đơn hàng"
        columns={userTableColumn}
        dataSource={listUser}
        bordered={true}
        rowKey="id"
      />

      <Modal
        title="Thêm sản phẩm"
        open={openModel}
        onCancel={handleCancel}
        maskClosable={false}
        onOk={() => {
          form
            .validateFields()
            .then((values) => {
              form.resetFields();
              handleSubmit(values);
            })
            .catch((info) => {
              console.log("Validate Failed:", info);
            });
        }}
      >
        <Form
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 14 }}
          layout="horizontal"
          onFinish={handleSubmit}
          form={form}
        >
          <Form.Item
            label="Tên sản phẩm"
            name="name"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tên!",
              },
            ]}
          >
            <Input placeholder="Nhập tên sản phẩm" />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
