import React, { useState } from "react";
import "../style/ecommerce.css";
import ProductItem from "../components/ecommerce/ProductItem";
import { productList } from "../utils/fakeData";
import Header from "../components/ecommerce/Header";
import { Button, Col, Modal, Row } from "antd";
import CardList from "../components/ecommerce/CartList";

export default function Ecommerce() {
  const [listProduct, setListProduct] = useState(productList);
  const [cartList, setCartList] = useState([]);
  const [isShowModal, setIsShowModal] = useState(false);

  const handleAddToCart = (product) => {
    const index = cartList.findIndex((item) => item.id === product.id);
    if (index > -1) {
      const newCartList = [...cartList];
      newCartList[index].quantity++;
      setCartList(newCartList);
      return;
    } else {
      const newCartList = [...cartList];
      newCartList.push({ ...product, quantity: 1 });
      setCartList(newCartList);
    }
  };

  const handleShowCartList = () => {
    setIsShowModal(true);
  };

  const handleChangeQuantity = (e, id) => {
    const newCartList = [...cartList];
    const index = newCartList.findIndex((item) => item.id === id);
    newCartList[index] = { ...newCartList[index], quantity: e.target.value };
    setCartList(newCartList);
  };

  return (
    <div className="wrapper-ecommerce">
      <Header
        totalProduct={cartList.length}
        handleShowCartList={handleShowCartList}
      />
      <h1 style={{ textAlign: "center" }}>Sản phẩm</h1>
      <div className="list-product container">
        {listProduct.map((product) => (
          <ProductItem
            key={product.id}
            productDetail={product}
            handleAddToCart={() => handleAddToCart(product)}
          />
        ))}
      </div>
      <Modal
        open={isShowModal}
        footer={<Footer />}
        onCancel={() => setIsShowModal(false)}
        width={800}
      >
        <Row>
          <Col span={24}>
            <CardList
              cartList={cartList}
              handleChangeQuantity={handleChangeQuantity}
            />
          </Col>
        </Row>
      </Modal>
    </div>
  );
}

const Footer = () => {
  return <Button type="primary" style={{ width: "100%" }}>Thanh Toán</Button>;
};
